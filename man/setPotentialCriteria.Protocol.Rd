% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/protocol.R
\name{setPotentialCriteria.Protocol}
\alias{setPotentialCriteria.Protocol}
\title{Sets potential criteria for homogenous modelling}
\usage{
\method{setPotentialCriteria}{Protocol}(sip, protocol)
}
\arguments{
\item{sip}{}

\item{protocol}{}
}
\value{
protocol
}
\description{
Sets potential criteria for homogenous modelling
}
\author{
Sascha Holzhauer
}
