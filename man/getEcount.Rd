% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/network_properties.R
\name{getEcount}
\alias{getEcount}
\title{Load number of edges}
\usage{
getEcount(sip, id, netid = "full")
}
\arguments{
\item{sip}{}

\item{id}{}
}
\value{
m
}
\description{
Load number of edges
}
\author{
Sascha Holzhauer
}
