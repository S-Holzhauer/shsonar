% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/algo_smallworldness.R
\name{swness_ws_analytical}
\alias{swness_ws_analytical}
\title{Determine small-world-ness analytically after Humphries2008}
\usage{
swness_ws_analytical(n, K, p, k = NULL)
}
\arguments{
\item{n}{}

\item{K}{}

\item{p}{}

\item{k}{}
}
\value{
swness
}
\description{
Determine small-world-ness analytically after Humphries2008
}
\author{
Sascha Holzhauer
}
