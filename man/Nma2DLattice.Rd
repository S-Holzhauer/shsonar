% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/approach2DLattice.R
\name{Nma2DLattice}
\alias{Nma2DLattice}
\title{Network Modelling Approach: 2D Lattice}
\usage{
Nma2DLattice(n)
}
\arguments{
\item{n}{}

\item{k}{}
}
\value{
Nma
}
\description{
Network Modelling Approach: 2D Lattice
}
\author{
Sascha Holzhauer
}
