% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/network_properties.R
\name{getModularityInfomap}
\alias{getModularityInfomap}
\title{Load modularity (infomap)}
\usage{
getModularityInfomap(sip, id, netid = "full")
}
\arguments{
\item{sip}{}

\item{id}{}
}
\value{
m
}
\description{
Load modularity (infomap)
}
\author{
Sascha Holzhauer
}
