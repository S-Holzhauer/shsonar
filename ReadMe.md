shsonar
=============

SimulatiOn Networks Assessment in R. The package provides a framework for analysing social network modelling components
and according approaches for their generation in agent-based modelling. The package implements the evaluation framework 
proposed in [1].

Feature include:

 * Assessment of Network Modelling Approaches regarding a large set of criteria:
 	* Representatvity
 	* Adjustability
 	* Conclusiveness
 	* Validity
 	* Efficiency
 * Implemented approaches comprise:
 	* Watts-Strogatz
 	* Barabasi-Albert
 	* Lattice networks
 	* WSH (Heterogeneous Watts-Strogatz; required NetGen)
	* HDFF (Homophily-based and Distance dependent Forest Fire approach; requires NetGen)
	
 * Generation of LaTeX tables for NCS (network class structure), ENC (empirical network class), PNC (produces network class),
 and scoring.

# Installation

``devtools::install_bitbucket("S-Holzhauer/shsonar@master")``


# First Steps

Start by reading the introduction vignette:  
``browseVignettes("shsonar")``


# Contact

If you're interested in using the package, or for discussions, questions and feature requests don't hesitate to contact
sascha.holzhauer@uni-kassel.de

 
[1] Holzhauer, S. Which network shall we choose? An evaluation framework to select social network modelling approaches for 
 	agent-based simulation. Proceedings of SSC 2015, 2015