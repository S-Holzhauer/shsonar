#' Identifies the number of communities at that level in the merge tree of a 
#' hierarchical community when the largest community has a size below the target size. 
#' @param community 
#' @param size target size 
#' @param startnumber
#' @param step 
#' @return number for which the 
#' 
#' @family network sampling
#' @seealso \code{\link[igraph]{cutat}}
#' @author Sascha Holzhauer
#' @export
identifyCommunitiesNumberBelowTargetSize <- function(community, size, startnumber = 1, step=10) {
	number = startnumber
	vcount <- community_walktrap$vcount
	repeat{
		comcut <- igraph::cutat(community, no=number)
		gc_size <- max(table(comcut))
		if(gc_size < size | number == vcount){
			break
		}
		number <- number + step
	}
	max(1, number - step)
}
#' Identify community closest/larger/smaller than given size
#' 
#' @details Goes down the merge tree of a hierarchical community until the level where the largest community is
#' below the target size. Uses a two-stage process where the the first one goes down by \code{approximatestep}, 
#' and the second one by 1.
#' In \code{mode = "closer"} it also checks the size of the larges community in the level above and returns the
#' closer result.
#' 
#' @param graph 
#' @param community 
#' @param size Preferred network size
#' @param step step for increasing the number of communities for which the cut is made 
#' 			(and at which the size of the largest is checked).
#' 			See \code{\link[igraph]{cutat}}.
#' @param mode
#' @return graph
#' 
#' @family network sampling
#' @author Sascha Holzhauer
#' @export
identifyCommunityOfSize <- function(graph, community, size, approximatestep = 100,
		mode = c("closer", "larger", "smaller")) {
	number1st <- identifyCommunitiesNumberBelowTargetSize(community, size = size, 
			startnumber = 1, step = approximatestep)
	number2nd <- identifyCommunitiesNumberBelowTargetSize(community, size = size, 
			startnumber = number1st, step = 1)
	comcut_higher <- igraph::cutat(community, no=number2nd)
	comcut_lower <- igraph::cutat(community, no=number2nd + 1)
	
	comcut <- comcut_higher
	if (mode == "closer") {
		if (max(table(comcut_higher)) - size > size - max(table(comcut_lower))) {
			comcut <- comcut_lower
		}
	}
	if (mode == "lower") {
		comcut <- comcut_lower
	}
	
	community_number <- names(table(comcut)[which(table(comcut) == max(table(comcut)))])
	g <- igraph::induced.subgraph(graph, which(comcut==community_number), impl="auto")
}
