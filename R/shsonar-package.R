#' SimulatiOn Networks Assessment in R
#'
#' \tabular{ll}{
#' Package \tab		shsonar \cr
#' Model:  \tab		<Basic> \cr
#' Licence:\tab		GPL v3 \cr
#' Be aware of:\tab	requires libraries futile.logger, igraph, R.oo, shdoe, shbasic, xtable \cr
#' Version: \tab	0.1.8 \cr
#' Date	   	\tab 	2015-05-02 \cr
#' Changes	\tab	refactoring, comment \cr
#' }
#' 
#' @description SimulatiOn Networks Assessment in R
#' @name  	shsonar
#' @aliases shsonar
#' @docType package
#' @title 	SimulatiOn Networks Assessment in R
#' @author 	Sascha Holzhauer, CESR/UoE \email{Sascha.Holzhauer@@ed.ac.uk}
#' @references TODO
#' @keywords social networks framwork assessment evaluation ABM agent-based modelling
#' 
NULL
.onAttach <- function(...) {
	packages = installed.packages()
	packageStartupMessage(paste("Welcome to shsonar ", packages[packages[,"Package"] == "shsonar", "Version"], sep=""))
}