#' Determines a specific parameter value that maximizes a given function of network properties.
#' TODO currently not used  / male optional in optimizeSingleParamWeighted
#' @param f function to minimize
#' @param n netwrok size
#' @param k average degree
#' @param initialp 
#' @param intervalhalflength 
#' @param numsamples 
#' @param ... 
#' @return p
#' 
#' @author Sascha Holzhauer
#' @export
optimizeParam <- function(f, n, k, initialp = 0.5, intervalhalflength = 0.2, numsamples = 10, ...) {
	
	shbasic::tic(type = "user.self")
	futile.logger::flog.info("Optimize p for S=%f, n=%d, k=%d", smallworldness, n, k)
	
	
	results <- c()
	cat("Sample: ")
	for (i in 1:numsamples) {
		cat(i,",", sep="")
		
		rootdata = tryCatch({	
					uniroot(f, c(initialp - intervalhalflength, initialp + intervalhalflength), extendInt="yes",
							smallworldness = smallworldness,...)
				}, error = function(e) {
					warning("NA introduced because of error in uniroot: ", e)
					list(root = NA)
				})
		results <- c(results, rootdata$root)
	}
	cat("\n")
	data.frame(p = sum(results)/numsamples, systime = toc()) 
}
#' Determine optimal parameter values for specific priority weights.
#' Also records runtime.
#' 
#' Deprecated: use \code{optimizeSingleParamWeighted} with numparam = 1 (default)
#' @param f approach-specific function whose 1st parameter is to optimise
#' @param repetitions 
#' @param weights vector of weights which are passed to f in turn
#' @param f_properties used to calculated network properties of networks with the optimised parameters
#' @param ... 
#' @return data.frame with columns weights, p, score, swness, modularity, systime
#' @author Sascha Holzhauer
#' 
#' @export
optimizeSingleParamWeighted<- function(f, f_properties, weights = c(0,1, 0.5), ...) {
	
	shbasic::tic(type = "user.self")
	arguments <- list(...)
	arguments <- arguments[!names(arguments) %in% c("dexp", "sip")]
	futile.logger::flog.info("Optimize function %s for %s", as.character(quote(f)), 
			paste(names(arguments), lapply(arguments, quiet(print)), collapse=", ", sep= "="))
	
	# TODO implement a wrapper object (consider ROI)
	
	futile.logger::flog.info("\n\n<--- Optimising single parameter using %s...",
				sip$optim$methodsingle,
				name = "shsonar.approach.tools.optimise")
		
	if (sip$optim$methodsingle == "crs2lm") {
		optresults = sapply(weights, FUN=nloptr::crs2lm, x0 = 0.5, fn=f, lower = c(0), upper = c(1),
				maxeval = sip$optim$maxevaluations, pop.size = sip$optim$popsize, ranseed = 					sip$optim$randomseed,
				xtol_rel = sip$optim$tol_relativechange, nl.info = sip$optim$shown_loptinfo, ...,
				simplify=FALSE)
		results <- sapply(optresults, function(x) {
					df <- list(minimum = x$par, objective = x$value)
				}, simplify=FALSE)
		
	} else if (sip$optim$methodsingle == "deoptim") {
		futile.logger::flog.info("Log file for optimisation: %s.\nScore function log goes to %s*.",
					sip$optim$clusterlog,
					sip$optim$scorefunlog,
					name = "shsonar.tools_optimiseParameter4property.R")
		
		shbasic::sh.ensurePath(outdir = sip$optim$clusterlog, stripFilename = TRUE)
		
		if (!exists(".Random.seed")) runif(1)
		storedRandomSeed <- .Random.seed
		set.seed(sip$optim$randomseed)
		
		Sys.sleep(60)
		
		optresults <- sapply(weights, FUN=shbasic::sh_tools_loadorsave,
				SIP = sip,
				OBJECTNAME = paste("ResultsOptimiseDEoptim", 
						if(!is.null(arguments$nma)) arguments$nma$shortname,
						paste(sapply(weights, function(x) 
											sprintf(fmt="%s%.3f", substr(names(x),1,3), x)), collapse="-"),
						as.character(quote(f)), 
						if(!is.null(arguments$scorefun)) gsub("/","-",gsub("[\\(\\)\\+\\*]","",arguments$scorefun)), 
						if(!is.null(arguments["properties"]))
							paste(sapply(setNames(arguments["properties"],NULL), sprintf, fmt="%.3f"), 
									collapse="-"),sep="_"),
				PRODUCTIONFUN = shsonar::DEoptim,
				fn=f,
				outfile = sip$optim$clusterlog,
				lower = c(0),
				upper = c(1),
				control = list(
						NP 			= sip$optim$popsize, 
						itermax 	= sip$optim$maxevaluations,
						trace 		= sip$optim$shown_loptinfo,
						reltol		= sip$optim$tol_relativechange,
						steptol 	= sip$optim$tol_steps,
						parallelType= sip$optim$paralleltype,
						packages	= list("shsonar", "shbasic"),
						parVar		= c("sip", "weights")
				),
				morecontrol = list("weights" = weights, "func" = arguments$scorefun,
						"properties" = arguments$properties, "nma" = arguments$nma$shortname),
				..., simplify=FALSE)

		results <- sapply(optresults, function(x) {
					df <- list(minimum =  setNames(x[["optim"]]$bestmem, NULL), objective = setNames(x[["optim"]]$bestval, NULL))
				}, simplify=FALSE)
	} else {
		results = sapply(weights, FUN=optimize, f=f, interval=c(0,1), tol=sip$optim$tol_relativechange, 
				..., simplify=FALSE)
	}
	
	Sys.sleep(60)
	print(results)
	
	if (!is.null(f_properties)) {
		properties = sapply(lapply(results, function(x) x[["minimum"]]), f_properties, ..., simplify=FALSE)
		return(data.frame(do.call(rbind, lapply(weights, function(x) unlist(x))), 
						p = do.call(rbind, lapply(results, function(x) unlist(x)))[,"minimum"], 
						score = do.call(rbind, lapply(results, function(x) unlist(x)))[,"objective"],
						do.call(rbind, lapply(properties, function(x) unlist(x))), 
						systime = shbasic::toc(), row.names = 1:length(weights)))
	} else {
		return(data.frame(do.call(rbind, lapply(weights, function(x) unlist(x))),
						p = do.call(rbind, lapply(results, function(x) unlist(x)))[,"minimum"], 
						score=do.call(rbind, lapply(results, function(x) unlist(x)))[,"objective"],
						systime = shbasic::toc(), row.names = 1:length(weights)))
	}
}
#' Determine optimal parameter values for specific priority weights.
#' Also records runtime.
#' @param f approach-specific function whose 1st parameter is to optimise
#' @param f_properties used to calculated network properties of networks with the optimised parameters
#' @param weights list of vectors of weights which are passed to f in turn
#' @param numparams number of parameters to optimise
#' @param ... finally passed to f 
#' @return data.frame with columns weights, parameters (\code{numparams} columns), score, swness, 
#' 			modularity, systime
#' @author Sascha Holzhauer
#' @export
optimizeMultiParamWeighted<- function(f, f_properties, weights, numparams = 1, NMA, ...) {
	
	getObjectDEoptimName <- function(weights, arguments, f) {
		paste("ResultsOptimiseDEoptim", 
				if(!is.null(arguments$nma)) arguments$nma$shortname,
				paste(sapply(weights, function(x) 
									sprintf(fmt="%s%.3f", substr(names(x),1,3), x)), collapse="-"),
				as.character(quote(f)), 
				if(!is.null(arguments$scorefun)) gsub("/","-",gsub("[\\(\\)\\+\\*]","",arguments$scorefun)), 
				if(!is.null(arguments["properties"]))
					paste(sapply(setNames(arguments["properties"],NULL), sprintf, fmt="%.3f"), 
							collapse="-"),sep="_")
	}
	
	shbasic::tic(type = "user.self")
	arguments <- list(...)
	arguments <- arguments[!names(arguments) %in% c("dexp", "sip")]
	futile.logger::flog.info("Optimize function %s for %s", as.character(quote(f)),
			paste(names(arguments), lapply(arguments, quiet(print)), collapse=", ", sep= "="))
	
	# TODO implement a wrapper object (consider ROI)
	
	futile.logger::flog.info("\n\n<--- Optimising multi parameter using %s...",
			sip$optim$methodsingle,
			name = "shsonar.approach.tools.optimise")
	
	if (sip$optim$methodsingle == "crs2lm") {
		optresults = sapply(weights, FUN=nloptr::crs2lm, x0 = rep(0.5, times = numparams), fn=f,
				lower = rep(0, times = numparams),
				upper = rep(1, times = numparams),
				maxeval = sip$optim$maxevaluations, pop.size = sip$optim$popsize,
				ranseed = sip$optim$randomseed,
				xtol_rel = sip$optim$tol_relativechange, nl.info = sip$optim$shown_loptinfo, ...,
				simplify=FALSE)
		results <- sapply(optresults, function(x) {
					df <- list(minimum = x$par, objective = x$value)
				}, simplify=FALSE)
		
	} else if (sip$optim$methodsingle == "deoptim") {
		shbasic::sh.ensurePath(outdir = sip$optim$clusterlog, stripFilename = TRUE)
		
		if (!exists(".Random.seed")) runif(1)
		storedRandomSeed <- .Random.seed
		set.seed(sip$optim$randomseed)
		
		Sys.sleep(15)

		futile.logger::flog.info("Considered Rdata object name: %s",
					getObjectDEoptimName(weights, arguments, f),
					name = "shsonar.tools_optimiseParameter4property.R")
			
		optresults <- sapply(weights, FUN=shbasic::sh_tools_loadorsave,
					SIP = sip,
					OBJECTNAME = getObjectDEoptimName(weights, arguments, f),
					PRODUCTIONFUN = shsonar::DEoptim,
					fn=f,
					outfile = sip$optim$clusterlog,
					lower = rep(0, times = numparams),
					upper = rep(1, times = numparams),
					control = list(
						NP 			= sip$optim$popsize, 
						itermax 	= sip$optim$maxevaluations,
						trace 		= sip$optim$shown_loptinfo,
						reltol		= sip$optim$tol_relativechange,
						steptol 	= sip$optim$tol_steps,
						parallelType= sip$optim$paralleltype,
						packages	= list("shsonar", "shbasic"),
						parVar		= c("sip", "weights")
					),
					morecontrol = list("weights" = weights, "func" = arguments$scorefun,
							"properties" = arguments$properties, "nma" = arguments$nma$shortname),
					..., simplify=FALSE)
		results <- sapply(optresults, function(x) {
					df <- list(minimum =  setNames(x[["optim"]]$bestmem, NULL), 
							  objective = setNames(x[["optim"]]$bestval, NULL))
				}, simplify=FALSE)
		.Random.seed <- storedRandomSeed
	} else {
		results = sapply(weights, FUN=optimize, f=f, interval=c(0,1), tol=sip$optim$tol_relativechange, 
				..., simplify=FALSE)
	}
	
	if (!is.null(f_properties)) {
		futile.logger::flog.info("Compute properties (protocol: %s, approach: %s)...",
				print(protocol),
				"NMA", #print(nma),
				name = "shsonar.approach.tools.properties")
		
		Sys.sleep(15)
		
		if(sip$prob$snowball) {
			library(snowfall) # could not find function "setDefaultClusterOptions"
			sip$netgen$numcpus <- sip$prob$netgen$numcpus
			snowfall::sfInit(parallel=TRUE, cpus = min(length(results),sip$prob$numcpus), 
							type = sip$prob$type,
							slaveOutfile = sprintf(sip$prob$logfile, "All"))
			snowfall::sfLibrary(shsonar)
			snowfall::sfLibrary(shbasic)
			snowfall::sfExport("sip")
			properties = snowfall::sfLapply(lapply(results, function(x) x[["minimum"]]), f_properties, ..., 
					simplify=FALSE)
			
			snowfall::sfRemoveAll()
			snowfall::sfStop()
			#if(sip$prob$type == "MPI")
			#	mpi.finalize()
			
		} else {
			properties = sapply(lapply(results, function(x) x[["minimum"]]), f_properties, ..., simplify=FALSE)
		}
		
		minimums <- lapply(results, function(x, nma) setNames(x[["minimum"]], 
							names(nma$parameters)), nma = NMA)
		
		return(data.frame(do.call(rbind, lapply(weights, function(x) unlist(x))), 
						do.call(rbind, minimums), 
						score = do.call(rbind, lapply(results, function(x) unlist(x)))[,"objective"],
						do.call(rbind, lapply(properties, function(x) unlist(x))), 
						systime = shbasic::toc(), row.names = 1:length(weights)))
	} else {
		minimums <- lapply(results, function(x, nma) setNames(x[["minimum"]], 
							names(nma$parameters)), nma = NMA)
		return(data.frame(do.call(rbind, lapply(weights, function(x) unlist(x))),
						do.call(rbind, minimums), 
						score=do.call(rbind, lapply(results, function(x) unlist(x)))[,"objective"],
						systime = shbasic::toc(), row.names = 1:length(weights)))
	}
}
quiet<-function(f){
	return(function(...) {capture.output(w<-f(...));return(w);});
}
