#' Calculates weighted score for given p regarding the match of given swness, modularity, and overall degree distribution.
#' Averages a defined number (repetitions) of networks.
#' 
#' @param p parameter that is optimised
#' @param weights named list (names need to be properties)
#' @param nma
#' @param protocol
#' @param properties
#' @param repetitions 
#' @param check_convergence 
#' @param print_convergence
#' @param dexp
#' @param max_repetitions
#' @param networkgenerator
#' @param scorefun
#' @param ... to capture parameters meant for optimisation function
#' @return score
#' 
#' @author Sascha Holzhauer
#' @export
scorefun_swness_modularity_dd <- function(p, weights, nma, sip, protocol, properties = NULL,
		repetitions = 10,
		check_convergence = FALSE, 
		print_convergence = FALSE,
		dexp,
		max_repetitions = repetitions * sip$optim$maxmeasurerepetitionsfactor,
		networkgenerator,
		# 2/(1 + e^(-0.5x))-1 when x may take negative values
		scorefun = "abs((swness) - smallworldness)/smallworldness * weights[['Smallworldness']] + 
				abs((m) - modularity)/modularity * weights[['Modularity (community: infomap)']] +
				dddeviation * weights[['Degree distribution']]") {
	
	smallworldness = NULL
	modularity = NULL
	if (!is.null(properties)) {
		smallworldness = properties[["Smallworldness"]] 
		modularity = properties[["Modularity (community: infomap)"]]
	}
	
	library(snowfall) # could not find function "setDefaultClusterOptions"
	
	## Error handling:
	if (!is.null(smallworldness) && smallworldness == 0 && grepl("/smallworldness",scorefun)) {
		R.oo::throw.default("Smallworldness may not be 0!")
	}
	if (!is.null(modularity) && modularity == 0 && grepl("/modularity",scorefun)) {
		R.oo::throw.default("Modularity may not be 0!")
	}
	
	futile.logger::flog.debug("<-- Start function scorefun_swness_modularity_dd with p = %s and weights %s...",
			paste(sprintf("%.3f", p), collapse="/"),
			paste(paste(substr(names(weights),1,6), sprintf("%.3f",weights),sep=":"), collapse="/"),
			name = "shsonar.approach.optimise.scorefun_swness_modularity_dd.scorefun_swness_modularity_dd")
	
	futile.logger::flog.debug("\tStart iteration for repetitions 1..%d with p = %s...",
			repetitions,
			paste(sprintf("%.3f", p), collapse="/"),
			name = "shsonar.approach.optimise.scorefun_swness_modularity_dd.scorefun_swness_modularity_dd"
	)
	graphs <- generateNetworks(sip = sip, nma = nma, protocol = protocol,
			parameters = list("p" = p),
			randomseeds = seq(1,repetitions,1))
	
	
	## Assess smallworldness
	swness <- 0
	values_swness <- c(0)
	if (!is.null(weights[['Smallworldness']]) && weights[['Smallworldness']] > 0) {
		snowfall::sfInit(parallel=(sip$scoring$numcpus > 1), 
				cpus = min(length(graphs), sip$scoring$numcpus))
		snowfall::sfLibrary(shsonar)
		values_swness <- snowfall::sfSapply(graphs, function(g) {shsonar::swness(g, apl_unconnected = TRUE, 
							montecarlo=FALSE)$sigma_analytical})
		snowfall::sfRemoveAll()
		snowfall::sfStop()
		
		swness <- mean(values_swness)
		
		
		futile.logger::flog.debug("Smallworldness: %f", swness,
				name = "shsonar.approach.optimise.scorefun_swness_modularity_dd.scorefun_swness_modularity_dd")
	}
	
	## Assess modularity
	m <- 0
	values_modularity <- c(0)
	if (!is.null(weights[['Modularity (community: infomap)']]) && 
			weights[['Modularity (community: infomap)']] > 0) {
		snowfall::sfInit(parallel=(sip$scoring$numcpus > 1), 
				cpus = min(length(graphs), sip$scoring$numcpus))
		snowfall::sfLibrary(shsonar)
		values_modularity <- snowfall::sfSapply(sapply(graphs, 
						igraph::infomap.community, simplify=FALSE), igraph::modularity)
		snowfall::sfRemoveAll()
		snowfall::sfStop()	
		
		m <- mean(values_modularity)
		
		
		futile.logger::flog.debug("Modularity (community: infomap): %f", m,
				name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
	}
	
	### Assess degree distribution
	dddeviation <- 0
	
	dddeviations <- c()
	if (!is.null(weights[['Degree distribution']]) && weights[['Degree distribution']] > 0) {
		
		
		
		local({
					dddeviations <<- sapply(graphs, function(graph) {
								# sum milieus for each repetition graph:
								sum(sapply(names(getProperty(sip, protocol, "Agent type composition")), function(milieu, g) {
													degrees = igraph::degree(graph = g, 
															v = igraph::V(g)[igraph::get.vertex.attribute(g,
																					name = sip$netgen$milieuattribute) == 
																			match(milieu, names(getProperty(sip, protocol, "Agent type composition")))],
															mode = "in")
													e1 = getProperty(sip, protocol, paste("Degree distribution", milieu))
													dddeviation = distrEx::HellingerDist(e1 = e1, e2 = degrees)
													
													
													
												}, g = graph, simplify = TRUE))	
							}, simplify = TRUE)
					
					
					dddeviation <<- mean(dddeviations)
				})
		futile.logger::flog.debug("DD deviation: %f", dddeviation,
				name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
	}
	
	if (check_convergence) {
		# apply DoE means
		data <- eval(parse(text=gsub("(swness)", "values_swness", 
								gsub("(m)", "values_modularity", scorefun, fixed=TRUE), fixed=TRUE)))
		
		futile.logger::flog.debug("Data: %s",
				capture.output(print(data)),
				name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
		
		required <- shdoe::shdoe_analyse_requiredNumberOfRuns(data, dexp, h = 1, name="NN")
		
		
		futile.logger::flog.info("Actually required repetitions for n=%d/p=%s: %d (performed: %d).",
				getProperty(sip, protocol, "Network size"),
				
				paste(sprintf("%.3f", p), collapse="/"),
				if (is.infinite(required)) NA else as.integer(required),
				as.integer(repetitions),
				name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
		
		
		
		num_repetitions = repetitions
		while (is.infinite(required) & num_repetitions < max_repetitions) {
			num_repetitions = num_repetitions + repetitions
			# extend number of graphs by repetitions:
			
			futile.logger::flog.debug("\tStart iteration for repetitions %d..%d with p = %s...",
					num_repetitions - repetitions,
					num_repetitions,
					
					paste(sprintf("%.3f", p), collapse="/"),
					name = "shsonar.approach.optimise.scorefun_swness_modularity_dd_pref_dist")
			
			
			graphs <- generateNetworks(sip = sip, nma = nma, protocol = protocol,
					parameters = list("p" = p),
					randomseeds = seq(1,repetitions,1))
			
			## Assess smallworldness
			if (!is.null(weights[['Smallworldness']]) && weights[['Smallworldness']] > 0) {
				snowfall::sfInit(parallel=(sip$scoring$numcpus > 1), cpus = min(length(graphs), sip$scoring$numcpus))
				snowfall::sfLibrary(shsonar)
				values_swness <- c(values_swness, snowfall::sfSapply(graphs, function(g) {
									shsonar::swness(g, apl_unconnected = TRUE, 
											montecarlo=FALSE)$sigma_analytical}))
				snowfall::sfRemoveAll()
				snowfall::sfStop()
				
				# calculate mean for final evaluation!
				swness <- mean(values_swness)
				
				
				futile.logger::flog.debug("Smallworldness: %f", swness,
						name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
			}
			
			
			
			## Assess modularity
			if (!is.null(weights[['Modularity (community: infomap)']]) && 
					weights[['Modularity (community: infomap)']] > 0) {
				snowfall::sfInit(parallel=(sip$scoring$numcpus > 1), cpus = min(length(graphs), sip$scoring$numcpus))
				snowfall::sfLibrary(shsonar)
				values_modularity <- c(values_modularity, snowfall::sfSapply(sapply(graphs, 
										igraph::infomap.community, simplify=FALSE), igraph::modularity))
				snowfall::sfRemoveAll()
				snowfall::sfStop()	
				
				m <- mean(values_modularity)
				
				
				futile.logger::flog.debug("Modularity (community: infomap): %f", m,
						name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
			}
			
			
			### Asses degree distribution
			dddeviation <- 0
			if (!is.null(weights[['Degree distribution']]) && weights[['Degree distribution']] > 0) {
				
				
				
				local({
							dddeviations <<- c(dddeviations, sapply(graphs, function(graph) {
												# sum milieus for each repetition graph:
												sum(sapply(names(getProperty(sip, protocol, "Agent type composition")), function(milieu, g) {
																	degrees = igraph::degree(graph = g, v = igraph::V(g)[igraph::get.vertex.attribute(g,
																									name = sip$netgen$milieuattribute) == 
																							match(milieu, names(getProperty(sip, protocol, "Agent type composition")))], mode = "in")
																	e1 = getProperty(sip, protocol, paste("Degree distribution", milieu))
																	
																	
																	dddeviation = distrEx::HellingerDist(e1 = e1, e2 = degrees)
																	
																}, g = graph, simplify = TRUE))	
											}, simplify = TRUE))
							
							dddeviation <<- mean(dddeviations)
						})
				futile.logger::flog.debug("DD deviation: %s", dddeviation,
						name = "shsonar.approach.optimise.scorefun_swness_modularity_dd_pref_dist")
			}
			
			
			data <- eval(parse(text=gsub("(swness)", "values_swness",gsub("(m)", "values_modularity", scorefun, 
											fixed=TRUE), fixed=TRUE)))
			
			futile.logger::flog.debug("Data: %s", capture.output(str(data)),
					name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
			
			if (any(is.na(data))) {
				futile.logger::flog.error("Data contains NA!",
						name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
			}
			
			required <- shdoe::shdoe_analyse_requiredNumberOfRuns(data, dexp, h = 1, name="NN")
			
			futile.logger::flog.info("Extended - actually required repetitions for n=%d/p=%s: %d (performed: %d).",
					getProperty(sip, protocol, "Network size"),
					
					paste(sprintf("%.3f", p), collapse="/"),
					if (is.infinite(required)) NA else as.integer(required),
					
					as.integer(repetitions),
					name = "shsonar.approach.optimise.scorefun_swness_modularity_dd")
		}
	}
	
	if (print_convergence) {
		filename <- sprintf("convergence_ws_p_swness_modularity__%d_%s_%f", 
				getProperty(sip, protocol, "Network size"),
				paste(sprintf("%.2f", p), collapse="-"),
				weight_sw)
		shdoe_visualise_requiredNumberOfRuns(list(data), dexp, filename=filename,
				useplot = TRUE)
	}
	result <- eval(parse(text=scorefun))
	futile.logger::flog.debug("...end function scorefun_swness_modularity_dd for p = %s with result %f --->",
			paste(sprintf("%.3f", p), collapse="/"),
			result,
			name = "shsonar.approach.optimise.scorefun_swness_modularity_dd.scorefun_swness_modularity_dd")
	
	sink()
	sink(type="message")
	
	names(result) <- NULL
	result
}
#' Calculate small-worldness, modulariy, and degree distribution deviation for given network parameters.
#' @param p 
#' @param nma NMA to determine network generation function
#' @param protocol
#' @param repetitions 
#' @return list of swness, modularity, and ddstatistic
#' 
#' @author Sascha Holzhauer
#' @export
calculatePropertiesSwMoDd <- function(p, sip, nma, protocol, repetitions = 10, ...) {
	graphs <- generateNetworks(sip = sip, nma = nma, protocol = protocol,
			parameters = list("p" = p),
			randomseeds = seq(1,repetitions,1))
	
	values_swness <- sapply(graphs, function(g) {shsonar::swness(g, apl_unconnected = TRUE, 
						montecarlo=FALSE)$sigma_analytical})
	values_modularity <- sapply(sapply(graphs, 
					igraph::infomap.community, simplify=FALSE), igraph::modularity)
	
	results <- list()
	results$swness 		<- mean(values_swness)
	results$modularity 	<- mean(values_modularity)
	
	dd <- sapply(graphs, igraph::degree.distribution, mode = "in", simplify = FALSE)
	suppressWarnings(ddmatrix <- do.call(rbind, dd))
	dd <- apply(ddmatrix, MARGIN= 2, FUN=mean)
	probs = distr::d(getProperty(sip, protocol, "Degree distribution all"))(
			0:(length(dd)-1))
	results$ddstatistic <-  chisq.test(dd, p = probs, rescale.p=TRUE)$statistic
	names(results$ddstatistic) <- NULL
	results
}
#' Get representativity data
#' @param sip 
#' @param dexp 
#' @param swindex
#' @param moindex 
#' @param protocol 
#' @param weight_sw  
#' @return data.frame with columns weights, p, score, systime
#' 
#' @author Sascha Holzhauer
optimiseRepresentativitySwMoDd <- function(sip, dexp, swindex = c("min", "max"), moindex = c("min", "max"), 
		nma, protocol, weights) {
	results <- shsonar::optimizeSingleParamWeighted(scorefun_swness_modularity_dd, f_properties = calculatePropertiesSwMoDd,
			sip = sip,
			weights = weights,
			properties = list(
					Smallworldness= getProperty(sip, protocol, "Smallworldness")[[swindex]],
					'Modularity (community: infomap)' = getProperty(sip, protocol, 
							"Modularity (community: infomap)")[[moindex]]),
			nma = nma,
			protocol = protocol,
			check_convergence = TRUE, dexp=dexp, repetitions = sip$optim$measurerepetitions)
}
#' Set dependent scores: relates the area enclosed by achievable values for modularity and smallworldness to
#' the desired area.
#'   
#' @param sip 
#' @param nma 
#' @param data 
#' @param firstName 
#' @param secondName 
#' @return nma
#' 
#' @author Sascha Holzhauer
#' @export
setDependentScores <- function(sip, nma, data, firstName, secondName, protocol) {
	
	m <- as.matrix(data)
	
	if (any(apply(m, MARGIN=2, FUN = function(x) length(unique(x)) == 1))) {
		column <- m[,!apply(m, MARGIN=2, function(x) min(x) == max(x))]
		score = (max(column) - min(column)) * sip$scoring$uniquevalues
	} else {
		score <- geometry::convhulln(m, options="FA")$vol / getMaxAreaDependentProperties(sip, protocol,
			firstName, secondName)
	}

	## set properties:
	value <- list(min = max(min(data$first), getProperty(sip, protocol, firstName)$min), 
			max = min(max(data$first), getProperty(sip, protocol, firstName)$max))
	
	if (value$min > value$max) {
		value <- 0
		score_first_above <- 0
	} else {
		score_first_above <- value$max - value$min
	}
	
	nma <- setApproachProperty(sip, nma, "Representativity", firstName, value)
	nma <- setApproachScore(sip, nma, category = "Representativity", property = firstName, value = score_first_above /  
			(getProperty(sip, protocol, firstName)$max - getProperty(sip, protocol, firstName)$min), 
			considerForMultiplication = FALSE)
	
	value <- list(min = max(min(data$second), getProperty(sip, protocol, secondName)$min), 
			max = min(max(data$second), getProperty(sip, protocol, secondName)$max))
	
	if (value$min > value$max) {
		value <- 0
		score_second_above <- 0
	} else {
		score_second_above <- value$max - value$min
	}
	
	nma <- setApproachProperty(sip, nma, "Representativity", secondName, value)
	nma <- setApproachScore(sip, nma, category = "Representativity", property = secondName, value = score_second_above /  
					(getProperty(sip, protocol, secondName)$max - getProperty(sip, protocol, secondName)$min), 
			considerForMultiplication = FALSE)
	
	nma <- setApproachScore(sip, nma, category = "Representativity", 
			property = paste("Combination (", firstName, " \\& ", secondName,")", sep=""), 
			score)
	nma
}
#' Area of convex hull around produced property points of dependent properties
#' @param sip 
#' @param protocol 
#' @param firstName 
#' @param secondName  
#' @return volume
#' 
#' @author Sascha Holzhauer
#' @export
getMaxAreaDependentProperties <- function(sip, protocol, firstName, secondName) {
	smallworldness= unlist(getProperty(sip, protocol, firstName))
	modularity = unlist(getProperty(sip, protocol, secondName))
	geometry::convhulln(as.matrix(expand.grid(smallworldness, modularity)), options="FA")$vol
}
#' Get maximum range for modularity
#' @param sip 
#' @param protocol  
#' @return data.frame with min/max values
#' 
#' @author Sascha Holzhauer
getMaxRangeModularity <- function(sip, nma, protocol) {
	futile.logger::flog.info("Determine max range modularity...",
			name = "approach.help.maxrange")
	
	weights <- list("Smallworldness" = 0,
			"Modularity (community: infomap)" = 1,
			"Degree distribution" = 0)
	
	results_min <- shsonar::optimizeSingleParamWeighted(scorefun_swness_modularity_dd, f_properties = NULL,
			weights = list(weights),
			check_convergence = TRUE, dexp=dexp, sip=sip, repetitions = sip$optim$measurerepetitions,
			nma = nma,
			protocol = protocol,
			scorefun = "(m)")
	results_max <- -1 * shsonar::optimizeSingleParamWeighted(scorefun_swness_modularity_dd, f_properties = NULL,
			weights = list(weights),
			check_convergence = TRUE, dexp=dexp, sip=sip, repetitions = sip$optim$measurerepetitions,
			nma = nma,
			protocol = protocol,
			scorefun = "-(m)")
	data.frame(min = results_min$score, max = results_max$score)
}
#' Get maximum range for Smallworldness
#' @param sip 
#' @param protocol  
#' @return data.frame with min/max values
#' 
#' @author Sascha Holzhauer
getMaxRangeSmallworldness <- function(sip, nma, protocol) {
	futile.logger::flog.info("Determine max range smallworldness...",
			name = "approach.help.maxrange")
	
	weights <- list("Smallworldness" = 1,
			"Modularity (community: infomap)" = 0,
			"Degree distribution" = 0)
	
	results_min <- shsonar::optimizeSingleParamWeighted(scorefun_swness_modularity_dd, f_properties = NULL,
			weights = list(weights),
			check_convergence = TRUE, dexp=dexp, sip=sip, repetitions = sip$optim$measurerepetitions,
			nma = nma,
			protocol = protocol,
			scorefun = "(swness)")
	results_max <- -1 * shsonar::optimizeSingleParamWeighted(scorefun_swness_modularity_dd, f_properties = NULL,
			weights = list(weights),
			check_convergence = TRUE, dexp=dexp, sip=sip, repetitions = sip$optim$measurerepetitions,
			nma = nma,
			protocol = protocol,
			scorefun = "-(swness)")
	data.frame(min = results_min$score, max = results_max$score)
}
#' Plot representativity (achievable characteristic space vs. desired characteristic space)
#' @param sip 
#' @param protocol 
#' @param data 
#' @param firstName 
#' @param secondName 
#' @param title 
#' @param filename  
#' @return plotted figure
#' 
#' @author Sascha Holzhauer
#' @export
visualiseRepresentativity <- function(sip, protocol, nma, data, firstName, secondName, 
		title = paste("Visualised Representativity for", nma$name, sep=" "),
		filename = gsub(" ", "_", title)) {
	
	library(methods)
	library(scales)
	
	p <- data.frame(data[grDevices::chull(data),], Type="possible")
	
	r <- expand.grid(unlist(getProperty(sip, protocol, firstName)),
			unlist(getProperty(sip, protocol, secondName)))
	colnames(r) <- c("first", "second")
	r <- data.frame(r[grDevices::chull(r),], Type="requested")
	d <- rbind(r,p)
	
	sip$fig$init(sip, outdir = paste(sip$dirs$output$figures, "lines", sep="/"), filename = filename)
	
	scaleColourElem <- ggplot2::scale_fill_manual( 
			values = c("possible" = "blue", "requested" = "orange"))
	
	p1 <- ggplot2::ggplot() +
			ggplot2::geom_polygon(data=d, alpha=.7, mapping=ggplot2::aes(x = first, y = second, 
							group=Type, fill=Type)) +
			scaleColourElem + 
			ggplot2::xlab(firstName) + ggplot2::ylab(secondName)
	if (!is.null(title)) ggplot2::labs(title = title) else NULL
	print(p1)
	sip$fig$close()
}
#' Plot representativity (achievable characteristic space vs. desired characteristic space)
#' @param sip
#' @param protocol 
#' @param firstName
#' @param secondName
#' @param firstNameShort
#' @param secondNameShort 
#' @param title 
#' @param colours list with NMA's shortname as names
#' @param filename  excluding directory
#' @return plotted figure
#' 
#' @author Sascha Holzhauer
#' @export
visualiseCompareRepresentativity <- function(sip, protocol, nmas, 
		firstName = "Modularity (community: infomap)",
		secondName = "Smallworldness",
		firstNameShort = "modularity", secondNameShort = "swness",
		title = paste("Visualised Representativity for", sapply(nmas, function(x) x$name), sep=" "),
		colours = NULL,
		filename = gsub(" ", "_", title)) {
	
	p <- lapply(nmas, function(nma) {
		shbasic::sh_tools_load(sip, paste("sw_possiblePropertyRange_", 
						stringr::str_to_lower(nma$shortname), sep=""))
		results <- get(paste("sw_possiblePropertyRange_", stringr::str_to_lower(nma$shortname), sep=""))
		data <- data.frame(first = results[[firstNameShort]],
				second = results[[secondNameShort]])
		p <- data.frame(data[grDevices::chull(data),], Type=paste(shbasic::shbasic_substitute(sip, "possible"), " ", 
						stringr::str_to_upper(nma$shortname), sep=""))
	})
	p <- do.call(rbind, p) 
	
	r <- expand.grid(unlist(getProperty(sip, protocol, firstName)),
			unlist(getProperty(sip, protocol, secondName)))
	colnames(r) <- c("first", "second")
	r <- data.frame(r[grDevices::chull(r),], Type=shbasic::shbasic_substitute(sip, "requested"))
	
	d <- rbind(r,p)
	
	sip$fig$init(sip, outdir = paste(sip$dirs$output$figures, "lines", sep="/"), filename = filename)
	
	if (!is.null(colours)) {
		names(colours) <- paste(shbasic::shbasic_substitute(sip, "possible"), " ", stringr::str_to_upper(names(colours)), sep="")
		scaleColourElem <- ggplot2::scale_fill_manual(name="",
				values = c(colours, setNames(sip$fig$colours$requestedarea, shbasic::shbasic_substitute(sip, "requested"))))
	} else {
		scaleColourElem <- ggplot2::scale_fill_discrete(name="")
	}
	
	p1 <- ggplot2::ggplot() +
			ggplot2::geom_polygon(data=d, alpha=.7, mapping=ggplot2::aes(x = first, y = second, 
							group=Type, fill=Type)) +
			scaleColourElem + 
			ggplot2::xlab(shbasic::shbasic_substitute(sip, firstName)) + 
			ggplot2::ylab(shbasic::shbasic_substitute(sip, secondName))
	if (!is.null(title)) ggplot2::labs(title = title) else NULL
	print(p1)
	sip$fig$close()
}
#' Stores given networks in subdir specified by
#' \code{sip$simulation[[stringr::str_to_lower(simulation$name)]]$dirs$networks} and the NMA name.
#' @param sip 
#' @param nma 
#' @param simulation 
#' @param networks 
#' @param numparams nuber of different parameters that are applied
#' @return stored networks, networkfilenames
#' 
#' @author Sascha Holzhauer
#' @export
storeSimulationNetworks <- function(sip, nma, simulation, networks, numparams) {
	
	simsip <<- sip$simulation[[stringr::str_to_lower(simulation$name)]]
	
	# generate filenames:
	filenames = paste(simsip$dirs$networks,
			"/", sip$sim$id,
			"/", nma$shortname,
			"/", sip$sim$id, "_", nma$shortname, "__", 
			do.call(paste, c(expand.grid(1:simsip$randomvariations, "_", 1:numparams), sep="")), 
				".graphml", sep="")
	
	shbasic::sh.ensurePath(paste(simsip$dirs$networks,"/", sip$sim$id,
					"/", nma$shortname, sep=""))
	
	# store networks
	futile.logger::flog.info("Store %d networks to %s...",
			length(networks),
			paste(simsip$dirs$networks,
					"/", sip$sim$id,
					"/", nma$shortname, sep=""),
			name="approach.ws.scores.conclusiveness")
	mapply(igraph::write.graph, 
			graph = networks,
			file = filenames,
			MoreArgs = list(format="graphml"))
	return(filenames)
}