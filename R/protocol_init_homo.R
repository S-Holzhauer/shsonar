#' ProtocolInitHomo
#' @param sip 
#' @param initfilename 
#' @param purpose e.g. simulation/application
#' @return protocol
#' 
#' @author Sascha Holzhauer
#' @export
ProtocolInitHomo <- function(sip, initfilename = sip$files$protocol$init, purpose = "") {
	me <- Protocol(sip, initfilename)
	me$name <- "Protocol (Initialisation: homogeneous)"
	me$purpose <- purpose
	class(me) <- c(append("ProtocolInitHomo", class(me)))
	return(me)
}
#' Sets potential (consider_homo == 1) criteria for homogenous modelling
#' @rdname ProtocolInitHomo
#' @param sip 
#' @param Protocol 
#' @return Protocol
#' 
#' @author Sascha Holzhauer
#' @export
setPotentialCriteria.ProtocolInitHomo <- function(sip, protocol) {
	criteria <- read.csv(protocol$initfilename, stringsAsFactors = FALSE)
	criteria <- criteria[criteria$consider_homo == 1,]
	protocol$criteriaPotential <- criteria
	return(protocol)
}
#' Sets relevant (weight > 0), ordered (sip$table$catsorder) criteria for homogeneous modelling
#' @rdname getRelevantCriteria
#' @param sip
#' \itemize{
#' 	\item sip$table$catsorder
#' } 
#' @param Protocol
#' @return Protocol
#' 
#' @author Sascha Holzhauer
#' @export
setRelevantCriteria.ProtocolInitHomo <- function(sip, protocol) {
	protocol <- setPotentialCriteria(sip, protocol)
	criteria <- protocol$criteriaPotential
	criteria <- protocol$criteria[criteria$Weight > 0, c("Category", "Parameter1", "Parameter2", "Weight")]
	criteria <- criteria[order(match(criteria$Category, sip$table$catsorder)),]	
	rownames(criteria) <- NULL
	protocol$criteriaRelevant <- criteria
	return(protocol)
}
